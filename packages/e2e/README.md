# E2E Tests

Start containers to use to write tests:

```sh
docker compose up -d
```

Setup Appsemble with the required organizations and app(s).

```sh
npx appsemble organization create appsemble --name 'Appsemble'  --context development --client-credentials 'test:test' --remote 'http://localhost:9999'
npx appsemble organization create eindhoven --name 'Eindhoven' --context development --client-credentials 'test:test' --remote 'http://localhost:9999'
npx appsemble block publish blocks/* --client-credentials 'test:test' --remote 'http://localhost:9999'
npx appsemble app publish apps/template --context development --client-credentials 'test:test' --remote 'http://localhost:9999'
```

Or you can set these values once in the config.

```sh
npx appsemble config set context review
npx appsemble config set remote "http://appsemble:9999"
npx appsemble config set client-credentials "test:test"
```

And then run

```sh
npx appsemble organization create appsemble --name 'Appsemble'
npx appsemble organization create eindhoven --name 'Eindhoven'
npx appsemble block publish blocks/*
npx appsemble app publish apps/*
```

To run end to end tests, run:

```sh
npm run test
```
