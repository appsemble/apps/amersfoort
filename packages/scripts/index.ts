import { configureLogger, handleError } from '@appsemble/node-utils';
import yargs, { type CommandModule } from 'yargs';

import * as seedAccount from './commands/seed-account.js';

yargs()
  .option('verbose', {
    alias: 'v',
    describe: 'Increase verbosity',
    type: 'count',
  })
  .option('quiet', {
    alias: 'q',
    describe: 'Decrease verbosity',
    type: 'count',
  })
  .middleware([configureLogger])
  .command(seedAccount as unknown as CommandModule)
  .demandCommand(1)
  .fail(handleError)
  .help()
  .parse(process.argv.slice(2));
