#!/bin/sh
[ -z "$GPG_PRIVATE_KEY" ] && echo "GPG_PRIVATE_KEY is required" && exit 1
[ -z "$GITLAB_ACCESS_TOKEN" ] && echo "GITLAB_ACCESS_TOKEN is required" && exit 1

gpg --import "$GPG_PRIVATE_KEY"
git config user.email bot@appsemble.com
git config user.name Appsemble
git commit --message "Set app IDs" --gpg-sign --cleanup whitespace

git push "https://appsemble-bot:$GITLAB_ACCESS_TOKEN@gitlab.com/appsemble/apps/amersfoort" "HEAD:refs/heads/main" \
  -o merge_request.create \
  -o merge_request.label=Chore \
  -o merge_request.remove_source_branch
