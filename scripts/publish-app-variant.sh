#!/bin/sh

APP=${1}
VARIANT=${2:-development}
CONTEXT=${3}

npx appsemble -vv app publish apps/$APP --context $VARIANT-$CONTEXT --resources --assets --modify-context
