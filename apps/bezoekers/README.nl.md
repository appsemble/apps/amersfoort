De bezoekersapp is ontwikkeld voor de gemeente Amersfoort. Met deze app kunnen medewerkers bij de
gemeente bezoeken inplannen en beheren.

## Rollen

Er zijn 2 rollen: medewerker en receptionist.

### Medewerker

Medewerkers van de gemeente kunnen een afspraak inplannen met een of meer bezoekers. Zowel de
medewerker zelf als de bezoekers krijgen een e-mail toegestuurd ter bevestiging.

Verder kunnen medewerkers hun eigen afspraken inzien en wijzigen.

### Receptionist

Als een bezoeker aankomt op locatie, dient deze zich bij de receptie te melden. De receptionist kan
hier via de app bijhouden welke bezoekers zich hebben aangemeld. De receptionist kan inzien met wie
de bezoeker een afspraak heeft en deze doorverwijzen naar de juiste locatie binnen het gebouw. Ook
is zo op elk moment bekend welke bezoekers in het gebouw aanwezig zijn.
